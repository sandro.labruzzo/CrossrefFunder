import json
with open("funder.csv", "wb") as w:
    w.write(f"ID\tNAME\tALTERNATIVE_NAMES\n".encode("utf-8"))
    with open("funders") as f:
        for line in f:
            data = json.loads(line)

            alt_names = ",".join(data['alt-names'])
            w.write(f"{data['id']}\t{data['name']}\t{alt_names}\n".encode("utf-8"))
