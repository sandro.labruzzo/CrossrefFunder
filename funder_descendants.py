import json
import requests

funder_roots = ['100000001', 
                '100010661', 
                '100011102',
                '501100001665',  
                '501100002341', #AKA
                '501100001602', #SFI
                '501100000923', #ARC
                '501100000038',
                '501100000155',
                '501100000024',
                '501100002848', #CONICYT
                '501100003448',
                '501100010198',
                '501100004564',
                '501100003407',
                '501100006588', 
                '501100004488', #HRZZ
                '501100006769', #RSF
                '501100001711',
                '501100004410',
                '100004440', # WT
                '100014013',
                '501100003246',
                '100000002',
                '501100000925',
                '501100002428',
                '501100001871',
                '501100001942',
                '100018693',
                '100018231',
                '501100016047', #SFRS
                '501100018877', #RIF
                '501100000046', #NRC
                '501100001659', #DFG
                '501100000806',#EEA
                '501100006364'] # INCA


f = {}

for funder in funder_roots:
    data = requests.get(f"https://api.crossref.org/funders/{funder}").json()    
    descendants = set(data['message']['descendants']).union(set(data['message']['replaces'])).union(set(data['message']['replaced-by']))
    print(f"root = {funder} -> {data['message']['name']}")
    root = f"10.13039/{data['message']['id']}"
    f[root] = [data['message']['name'], data['message']['replaces'], data['message']['replaced-by'], []]
    while len(descendants) > 0:
        descendant = descendants.pop()
        data = requests.get(f"https://api.crossref.org/funders/{descendant}").json()
        key = f"10.13039/{data['message']['id']}"
        if key in f:
            continue
        print(f"descendant = {descendant} -> {data['message']['name']}")
        f[root][3].append([data['message']['name'], data['message']['id']])
        f[key] = [data['message']['name'], data['message']['replaces'], data['message']['replaced-by'], 
    set(data['message']['descendants']).union(set(data['message']['replaces'])).union(set(data['message']['replaced-by']))]
        descendants = descendants.union(set(data['message']['descendants']).union(set(data['message']['replaces'])).union(set(data['message']['replaced-by'])))


fout = open('funders_desc.tsv','w')

fout.write(f"NAME\tDOI\tDESCENDANT NAME\tDESCENDANT DOI\n")
for key in funder_roots:
    funder = f[f"10.13039/{key}"]
    fout = open(f"{funder[0]}.tsv",'w')
    fout.write(f"NAME\tDOI\tDESCENDANT NAME\tDESCENDANT DOI\n")
    fout.write(f"{funder[0]}\t10.13039/{key}\t\t\n")
    for d in funder[3]:
        fout.write(f"\t\t{d[0]}\t10.13039/{d[1]}\n")
    fout.close()