import requests
import json

def retrieveCrossrefFunder(rows=1000, offset=0):
    print(f"requesting https://api.crossref.org/funders?rows={rows}&offset={offset}")
    data = requests.get(f"https://api.crossref.org/funders?rows={rows}&offset={offset}")
    return data.json()
    



with open("funders", "w") as f:    
    data = retrieveCrossrefFunder()
    total = 0
    total_result = data['message']['total-results']
    for item in data['message']['items']:
        f.write(json.dumps(item))
        total += 1
        f.write("\n")        
    while total< total_result:
        data = retrieveCrossrefFunder(offset=total)
        for item in data['message']['items']:
            f.write(json.dumps(item))
            total += 1
            f.write("\n")



